import { useState, useEffect } from "react";

function ShoesList(props) {
    const [shoes, setShoes] = useState([])
    const deleteShoe = async (event, id) => {
        event.preventDefault()
        const url = `http://localhost:8080/api/shoes/${id}`
        const response = await fetch(url, { method: "DELETE" })
        if (response.ok) {
            setShoes(shoes.filter((shoe) => shoe.id !== id))
        }
    }
    useEffect(() => {
        async function loadShoes() {
            const response = await fetch("http://localhost:8080/api/shoes")
            if (response.ok) {
                const data = await response.json()
                setShoes(data.shoes)
                console.log("Meow.")

            }
            else {
                console.error(response)
            }
        }
        loadShoes()
    }, [])
    return (
        <div className="col">
            {props.shoes.map((shoe) => {
                return (
                    <div key={shoe.id} className="card mb-3 shadow">
                        <img src={shoe.picture_url} width="400" alt={shoe.model_name} className="card-img-top" />
                        <div className="card-body">
                            <label htmlFor="model_name">Model Name</label>
                            <h5 className="card-title">{shoe.model_name}</h5>
                            <label htmlFor="color">Color</label>
                            <h6 className="card-text text-muted">{shoe.color}</h6>
                            <label htmlFor="manufacturer">Manufacturer</label>
                            <h6 className="card-text text-muted">{shoe.manufacturer}</h6>
                            <label htmlFor="closet_name">Closet</label>
                            <h6 className="card-text text-muted">{shoe.bin.closet_name}</h6>
                            <button className="btn btn-danger" onClick={(event) => deleteShoe(event, shoe.id)}>Delete</button>
                        </div>
                    </div>);
            })}
        </div>
    )
}

export default ShoesList