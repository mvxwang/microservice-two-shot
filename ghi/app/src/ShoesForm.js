import { useState, useEffect } from "react";

function ShoesForm() {
    const [manufacturer, setManufacturer] = useState("")
    const [modelName, setModelName] = useState("")
    const [color, setColor] = useState("")
    const [pictureUrl, setPictureUrl] = useState("")
    const [bins, setBins] = useState([])
    const [bin, setBin] = useState("")
    const handleManufacturerChange = (event) => {
        const manufacturer = event.target.value
        setManufacturer(manufacturer)
    }
    const handleNameChange = (event) => {
        const model_name = event.target.value
        setModelName(model_name)
    }
    const handleColorChange = (event) => {
        const color = event.target.value
        setColor(color)
    }
    const handlePictureChange = (event) => {
        const picture_url = event.target.value
        setPictureUrl(picture_url)
    }
    const handleBinChange = (event) => {
        const bin = event.target.value
        setBin(bin)
    }
    const fetchData = async () => {
        const url = "http://localhost:8100/api/bins";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);
    console.log(bins)

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.manufacturer = manufacturer
        data.model_name = modelName
        data.color = color
        data.picture_url = pictureUrl
        data.bin = bin
        console.log(data)
        const shoeUrl = "http://localhost:8080/api/shoes/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        }
        const response = await fetch(shoeUrl, fetchConfig)
        if (response.ok) {
            const newShoe = await response.json()
            console.log(newShoe)

            setManufacturer('')
            setColor('')
            setModelName('')
            setPictureUrl('')
            setBin('')
        }
    }
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new shoe</h1>
                    <form onSubmit={handleSubmit} id="create-shoe-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleManufacturerChange} value={manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                            <label htmlFor="manufacturer">Manufacturer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} value={modelName} placeholder="Model Name" required type="text" name="modelName" id="modelName" className="form-control" />
                            <label htmlFor="modelName">Model Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleColorChange} value={color} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePictureChange} value={pictureUrl} placeholder="pictureUrl" required type="text" name="picture_url" id="picture_url" className="form-control" />
                            <label htmlFor="picture_url">Picture URL</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleBinChange} value={bin} required name="bin" id="bin" className="form-select">
                                <option value="">Choose a bin</option>
                                {bins.map(bin => {
                                    return (
                                        <option key={bin.id} value={bin.href}>
                                            {bin.closet_name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default ShoesForm