# Wardrobify

Team:

* Max Wang - Shoes Microservice!

* Alan Y.C. Cheng - Hats Microservice!

## Design

## Shoes Microservice

Explain your models and integration with the wardrobe
microservice, here.

For the Shoes Microservice, I created RESTful APIS to get a list of shoes, create a shoe, and delete a shoe with respect to the wardrobe, and it's connection to the shoes being the bin they are located in. I also created React Components such as a ShoesForm and a ShoesList to display a form to create a shoe, as well as a list view of all shoes currently in the database. In my ShoesList React Component I also included functionality to delete a shoe directly from the database. Finally, I routed all my components together in the Nav.js and App.js.

## Hats Microservice

Explain your models and integration with the wardrobe
microservice, here.
